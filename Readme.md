4th Year Course - Visual Computing
-------------------------------------------

Project Members
---------------
Simão Reis 

Miguel Valério

Project Description
-------------------
Implementation of multiple methods of Template Matching (find the presence of a given sub-image).
Algorithms Implemented:
* SQDIFF - Square Difference between Pixels
```math
R(x,y)=\sum_{x',y'}(T(x',y')-I(x+x',y+y'))^2
```
* CCORR - Correlation between pixels.
```math
R(x,y)=\sum_{x',y'}T(x',y')I(x+x',y+y')
```
* CCOEFF - Weighted correlation between pixels.
```math
R(x,y)=\sum_{x',y'}T'(x',y')I'(x,x',y,y')
```

Normalized Forms:
```math
N(x,y)=\frac{R(x,y)}{\sqrt{\sum_{x',y'}T(x',y')^2\sum_{x',y'}I(x+x',y+y')^2}}
```

Implementation
--------------
OpenCV