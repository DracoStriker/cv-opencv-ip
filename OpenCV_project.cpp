
/**
 * \author Simão Reis
 * \author Miguel Valério
 * \version 1.0
 * \name Template Matching
 * */

// System Libraries

#include <iostream>

// OpenCV Libraries

#include "opencv2/core/core.hpp"

#include "opencv2/imgproc/imgproc.hpp"

#include "opencv2/highgui/highgui.hpp"

// Namespaces

using namespace cv;

using namespace std;

// Global Constants

const char  *janelaImagemOriginal  = "Imagem Original",
			*janelaImagemTemplate  = "Imagem Template",
			*janelaImagemResultado = "Imagem Resultado",
			*janelaImagemAlgoritmo = "Imagem Algoritmo",
			*trackbar = "Matching Algorithm",
			*threshholdbar = "Threshold";
			

// Global Variables

Mat imagemOriginal,
	imagemTemplate,
	imagemResultado,
	imagemAlgoritmo;
	
Rect box;
	
int method;

int drawing_box;

float thresholdMax;
	
// Auxiliar Functions

void tempMatch()
{
	int res_cols = imagemOriginal.cols - imagemTemplate.cols + 1;

	int res_rows = imagemOriginal.rows - imagemTemplate.rows + 1;

	imagemAlgoritmo.create(res_cols, res_rows, CV_32FC1);

	imagemResultado = imagemOriginal.clone();

	matchTemplate(imagemOriginal, imagemTemplate, imagemAlgoritmo, method);

	normalize(imagemAlgoritmo, imagemAlgoritmo, 0, 1, NORM_MINMAX, -1, Mat());
	
	if(method == CV_TM_SQDIFF || method == CV_TM_SQDIFF_NORMED)
	{
		subtract(255, imagemAlgoritmo, imagemAlgoritmo);

		normalize(imagemAlgoritmo, imagemAlgoritmo, 0, 1, NORM_MINMAX, -1, Mat());
	}

	double minVal; double maxVal; Point minLoc; Point maxLoc;

	while(true)
	{
		minMaxLoc( imagemAlgoritmo, &minVal, &maxVal, &minLoc, &maxLoc );
		
		if ( maxVal > thresholdMax )
		{
			rectangle( imagemResultado, maxLoc, Point( maxLoc.x + imagemTemplate.cols , maxLoc.y + imagemTemplate.rows ), Scalar::all(0), 2, 8, 0 );
			
			rectangle( imagemAlgoritmo, maxLoc, Point( maxLoc.x + imagemTemplate.cols , maxLoc.y + imagemTemplate.rows ), Scalar::all(0), 2, 8, 0 );
		}
		else
			break;
	}

	imshow(janelaImagemResultado, imagemResultado);

	imshow(janelaImagemAlgoritmo, imagemAlgoritmo);
}

// Callback functions

void changeAlgorithm( int value, void *data )
{
	if (imagemTemplate.data)
	{
		tempMatch();
	}
}

void changeThreshold( int value, void *data )
{
	thresholdMax = (float) value / 100;
}

void mouseCallback( int event, int x, int y, int flags, void* param )
{
	Size size;
	
	switch ( event )
	{
		case CV_EVENT_LBUTTONDOWN:
			
			box = Rect( x, y, 0, 0 );
			
			drawing_box = 1;
			
			break;
			
		case CV_EVENT_MOUSEMOVE:
		
			if ( drawing_box )
			{
				box.width = x - box.x;
			
				box.height = y - box.y;
			}
			
			break;
			
		case CV_EVENT_LBUTTONUP:
		
			drawing_box = 0;
			
			size = imagemOriginal.size();
			
			if( box.width < 0 )
			{
				box.x += box.width;
				
				box.width *= -1;
			}
			
			if (box.x < 0)
			{
				box.width += box.x;
				
				box.x = 0;
			}
			
			if ( (box.x + box.width) > size.width )
			{
				box.width = size.width - box.x;
			}
			
			if( box.height < 0 )
			{
				box.y += box.height;
				
				box.height *= -1;
			}
			
			if (box.y < 0)
			{
				box.height += box.y;
				
				box.y = 0;
			}
			
			if ( (box.y + box.height) > size.height )
			{
				box.height = size.height - box.y;
			}
			
			if ((box.width != 0) && (box.height != 0))
			{
				imagemTemplate = imagemOriginal( box );
				
				imshow( janelaImagemTemplate, imagemTemplate );
				
				tempMatch();
			}
			
			break;
	}
}

// Main function

int main( int argc, char** argv )
{   
	Size size, tempSize;
	
	int threshold;
	
	if( argc < 2 )
    {
        cout << "Falta o nome do ficheiro da imagem !!" << endl;

        return -1;
    }
    
    if ( argc > 3 )
    {
		cout << "Demasiados argumentos !!" << endl;

        return -1;
	}
    
    imagemOriginal = imread( argv[1], CV_LOAD_IMAGE_UNCHANGED );
    
    if( ! imagemOriginal.data )
	{
	    cout << "Ficheiro da imagem original nao foi aberto ou localizado !!" << endl;

	    return -1;
	}
	
	if ( argc == 3 )
	{
		imagemTemplate = imread( argv[2], CV_LOAD_IMAGE_UNCHANGED );
		
		if( ! imagemTemplate.data )
		{
			cout << "Ficheiro da imagem template nao foi aberto ou localizado !!" << endl;
			
			return -1;
		}
		
		size = imagemOriginal.size();
		
		tempSize = imagemTemplate.size();
		
		if ( (tempSize.width > size.width) || (tempSize.height > size.height) )
		{
			cout << "Ficheiro da imagem template é maior do que a imagem original !!" << endl;
			
			return -1;
		}
	}
	
	// Algoritmo por omissao
	
	method = CV_TM_CCOEFF;
	
	// Threshhold por omissao
	
	thresholdMax = 0.99;

	// Janela Imagem Original

    namedWindow( janelaImagemOriginal, CV_WINDOW_AUTOSIZE );
    
    moveWindow(janelaImagemOriginal, 0, 0);
    
    createTrackbar( trackbar, janelaImagemOriginal, &method, 5, changeAlgorithm );
    
    threshold = (int) (thresholdMax * 100);
    
    createTrackbar( threshholdbar, janelaImagemOriginal, &threshold, 100, changeThreshold );

    imshow( janelaImagemOriginal, imagemOriginal );

    // Janela Imagem Template
    
    namedWindow( janelaImagemTemplate, CV_WINDOW_AUTOSIZE );
    
    moveWindow(janelaImagemTemplate, 300, 0);
    
	if ( argc > 2 )
	{
		imshow( janelaImagemTemplate, imagemTemplate );
	}

	// Janela Imagem Resultado e Algoritmo
	
	namedWindow( janelaImagemResultado, CV_WINDOW_AUTOSIZE );
	
	moveWindow(janelaImagemResultado, 600, 0);

	namedWindow( janelaImagemAlgoritmo, CV_WINDOW_AUTOSIZE );
	
	moveWindow(janelaImagemAlgoritmo, 900, 0);
	
	// Template Matching
	
	if ( argc > 2 )
	{
		tempMatch();
	}
	
	// CallBacks
	
	setMouseCallback( janelaImagemOriginal, mouseCallback, NULL);
	
	// Sub Imagem
	
	drawing_box = 0;
	
	// Esq para sair

    while ( waitKey( 0 ) != 27 );
    
    // Destruir as janelas
    
    destroyAllWindows();
}

